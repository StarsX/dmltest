//*********************************************************
//
// Copyright (c) Microsoft. All rights reserved.
// This code is licensed under the MIT License (MIT).
// THIS CODE IS PROVIDED *AS IS* WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING ANY
// IMPLIED WARRANTIES OF FITNESS FOR A PARTICULAR
// PURPOSE, MERCHANTABILITY, OR NON-INFRINGEMENT.
//
//*********************************************************

#include "DMLTest.h"

using namespace std;
using namespace XUSG;
using namespace XUSG::ML;

DMLTest::DMLTest(uint32_t width, uint32_t height, std::wstring name) :
	DXFramework(width, height, name),
	m_frameIndex(0),
	m_showFPS(true),
	m_pausing(false)
{
#if defined (_DEBUG)
	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	AllocConsole();
	FILE* stream;
	freopen_s(&stream, "CONOUT$", "w+t", stdout);
	freopen_s(&stream, "CONIN$", "r+t", stdin);
#endif
}

DMLTest::~DMLTest()
{
#if defined (_DEBUG)
	FreeConsole();
#endif
}

void DMLTest::OnInit()
{
	LoadPipeline();
	LoadAssets();
}

// Load the rendering pipeline dependencies.
void DMLTest::LoadPipeline()
{
	auto dxgiFactoryFlags = 0u;

#if defined(_DEBUG)
	// Enable the debug layer (requires the Graphics Tools "optional feature").
	// NOTE: Enabling the debug layer after device creation will invalidate the active device.
	{
		com_ptr<ID3D12Debug1> debugController;
		if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugController))))
		{
			debugController->EnableDebugLayer();
			//debugController->SetEnableGPUBasedValidation(TRUE);

			// Enable additional debug layers.
			dxgiFactoryFlags |= DXGI_CREATE_FACTORY_DEBUG;
		}
	}
#endif

	com_ptr<IDXGIFactory4> factory;
	ThrowIfFailed(CreateDXGIFactory2(dxgiFactoryFlags, IID_PPV_ARGS(&factory)));

	DXGI_ADAPTER_DESC1 dxgiAdapterDesc;
	com_ptr<IDXGIAdapter1> dxgiAdapter;
	auto hr = DXGI_ERROR_UNSUPPORTED;
	for (auto i = 0u; hr == DXGI_ERROR_UNSUPPORTED; ++i)
	{
		dxgiAdapter = nullptr;
		ThrowIfFailed(factory->EnumAdapters1(i, &dxgiAdapter));
		hr = D3D12CreateDevice(dxgiAdapter.get(), D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&m_device));
	}

	dxgiAdapter->GetDesc1(&dxgiAdapterDesc);
	if (dxgiAdapterDesc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE)
		m_title += dxgiAdapterDesc.VendorId == 0x1414 && dxgiAdapterDesc.DeviceId == 0x8c ? L" (WARP)" : L" (Software)";
	ThrowIfFailed(hr);

	// Create the command queue.
	N_RETURN(m_device->GetCommandQueue(m_commandQueue, CommandListType::DIRECT, CommandQueueFlags::NONE), ThrowIfFailed(E_FAIL));

	// Describe and create the swap chain.
	DXGI_SWAP_CHAIN_DESC1 swapChainDesc = {};
	swapChainDesc.BufferCount = FrameCount;
	swapChainDesc.Width = m_width;
	swapChainDesc.Height = m_height;
	swapChainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapChainDesc.SampleDesc.Count = 1;

	com_ptr<IDXGISwapChain1> swapChain;
	ThrowIfFailed(factory->CreateSwapChainForHwnd(
		m_commandQueue.get(),		// Swap chain needs the queue so that it can force a flush on it.
		Win32Application::GetHwnd(),
		&swapChainDesc,
		nullptr,
		nullptr,
		&swapChain
	));

	// This sample does not support fullscreen transitions.
	ThrowIfFailed(factory->MakeWindowAssociation(Win32Application::GetHwnd(), DXGI_MWA_NO_ALT_ENTER));

	ThrowIfFailed(swapChain.As(&m_swapChain));
	m_frameIndex = m_swapChain->GetCurrentBackBufferIndex();

	m_descriptorTableCache.SetDevice(m_device);

	// Create frame resources.
	// Create a RTV and a command allocator for each frame.
	for (auto n = 0u; n < FrameCount; ++n)
	{
		N_RETURN(m_renderTargets[n].CreateFromSwapChain(m_device, m_swapChain, n), ThrowIfFailed(E_FAIL));
		N_RETURN(m_device->GetCommandAllocator(m_commandAllocators[n], CommandListType::DIRECT), ThrowIfFailed(E_FAIL));
	}
}

// Load the sample assets.
void DMLTest::LoadAssets()
{
	m_pipelineLayoutCache.SetDevice(m_device);
	m_graphicsPipelineCache.SetDevice(m_device);
	m_computePipelineCache.SetDevice(m_device);
	m_descriptorTableCache.SetDevice(m_device);

	// Create the command list.
	N_RETURN(m_device->GetCommandList(m_commandList.GetCommandList(), 0, CommandListType::DIRECT,
		m_commandAllocators[m_frameIndex], nullptr), ThrowIfFailed(E_FAIL));

	N_RETURN(InitTensors(), ThrowIfFailed(E_FAIL));

	{
		Util::PipelineLayout pipelineLayout;
		pipelineLayout.SetRange(0, DescriptorType::ROOT_UAV, 1, 0,
			0, DescriptorRangeFlag::DATA_STATIC_WHILE_SET_AT_EXECUTE);
		X_RETURN(m_pipelineLayouts[0], pipelineLayout.GetPipelineLayout(m_pipelineLayoutCache,
			PipelineLayoutFlag::DENY_VERTEX_SHADER_ROOT_ACCESS |
			PipelineLayoutFlag::DENY_HULL_SHADER_ROOT_ACCESS |
			PipelineLayoutFlag::DENY_DOMAIN_SHADER_ROOT_ACCESS |
			PipelineLayoutFlag::DENY_GEOMETRY_SHADER_ROOT_ACCESS |
			PipelineLayoutFlag::DENY_PIXEL_SHADER_ROOT_ACCESS,
			L"FillTensorLayout"), ThrowIfFailed(E_FAIL));
	}

	{
		Util::PipelineLayout pipelineLayout;
		pipelineLayout.SetRange(0, DescriptorType::CONSTANT, 2, 0);
		pipelineLayout.SetRange(1, DescriptorType::ROOT_SRV, 1, 0);
		pipelineLayout.SetShaderStage(0, Shader::Stage::PS);
		pipelineLayout.SetShaderStage(1, Shader::Stage::PS);
		X_RETURN(m_pipelineLayouts[1], pipelineLayout.GetPipelineLayout(m_pipelineLayoutCache,
			PipelineLayoutFlag::DENY_VERTEX_SHADER_ROOT_ACCESS |
			PipelineLayoutFlag::DENY_HULL_SHADER_ROOT_ACCESS |
			PipelineLayoutFlag::DENY_DOMAIN_SHADER_ROOT_ACCESS |
			PipelineLayoutFlag::DENY_GEOMETRY_SHADER_ROOT_ACCESS,
			L"TensorToImageLayout"), ThrowIfFailed(E_FAIL));
	}

	{
		const auto cs = m_shaderPool.CreateShader(Shader::Stage::CS, 0, L"CSFillTensor.cso");
		N_RETURN(cs, ThrowIfFailed(E_FAIL));

		Compute::State state;
		state.SetPipelineLayout(m_pipelineLayouts[0]);
		state.SetShader(cs);
		X_RETURN(m_pipelines[0], state.GetPipeline(m_computePipelineCache, L"FillTensor"), ThrowIfFailed(E_FAIL));
	}

	{
		const auto vs = m_shaderPool.CreateShader(Shader::Stage::VS, 0, L"VSTensorToImage.cso");
		const auto ps = m_shaderPool.CreateShader(Shader::Stage::PS, 0, L"PSTensorToImage.cso");
		N_RETURN(vs, ThrowIfFailed(E_FAIL));
		N_RETURN(ps, ThrowIfFailed(E_FAIL));

		Graphics::State state;
		state.SetPipelineLayout(m_pipelineLayouts[1]);
		state.SetShader(Shader::Stage::VS, vs);
		state.SetShader(Shader::Stage::PS, ps);
		state.DSSetState(Graphics::DEPTH_STENCIL_NONE, m_graphicsPipelineCache);
		state.IASetPrimitiveTopologyType(PrimitiveTopologyType::TRIANGLE);
		state.OMSetNumRenderTargets(1);
		state.OMSetRTVFormat(0, Format::R8G8B8A8_UNORM);
		X_RETURN(m_pipelines[1], state.GetPipeline(m_graphicsPipelineCache, L"TensorToImage"), ThrowIfFailed(E_FAIL));
	}

	//m_voxelizer = make_unique<Voxelizer>(m_device);
	//if (!m_voxelizer) ThrowIfFailed(E_FAIL);

	//vector<Resource> uploaders(0);
	//if (!m_voxelizer->Init(m_commandList, m_width, m_height,
		//m_renderTargets[0].GetResource()->GetDesc().Format,
		//m_depth.GetResource()->GetDesc().Format, uploaders))
		//ThrowIfFailed(E_FAIL);

	// Close the command list and execute it to begin the initial GPU setup.
	ThrowIfFailed(m_commandList.Close());
	BaseCommandList* const ppCommandLists[] = { m_commandList.GetCommandList().get() };
	m_commandQueue->ExecuteCommandLists(static_cast<uint32_t>(size(ppCommandLists)), ppCommandLists);

	// Create synchronization objects and wait until assets have been uploaded to the GPU.
	{
		N_RETURN(m_device->GetFence(m_fence, m_fenceValues[m_frameIndex]++, FenceFlag::NONE), ThrowIfFailed(E_FAIL));

		// Create an event handle to use for frame synchronization.
		m_fenceEvent = CreateEvent(nullptr, FALSE, FALSE, nullptr);
		if (m_fenceEvent == nullptr)
		{
			ThrowIfFailed(HRESULT_FROM_WIN32(GetLastError()));
		}

		// Wait for the command list to execute; we are reusing the same command 
		// list in our main loop but for now, we just want to wait for setup to 
		// complete before continuing.
		WaitForGpu();
	}
}

bool DMLTest::InitTensors()
{
	DML_CREATE_DEVICE_FLAGS dmlCreateDeviceFlags = DML_CREATE_DEVICE_FLAG_NONE;

#if defined (_DEBUG)
	// If the project is in a debug build, then enable debugging via DirectML debug layers with this flag.
	dmlCreateDeviceFlags |= DML_CREATE_DEVICE_FLAG_DEBUG;
#endif

	ThrowIfFailed(DMLCreateDevice(m_device.get(), dmlCreateDeviceFlags, IID_PPV_ARGS(&m_mlDevice)));

	{
		// Create DirectML operator(s). Operators represent abstract functions such as "multiply", "reduce", "convolution", or even
		// compound operations such as recurrent neural nets. This example creates an instance of the Identity operator,
		// which applies the function f(x) = x for all elements in a tensor.
		Tensor tensor;
		const uint32_t tensorSizes[] = { 1, m_width, m_height, 1 };
		m_tensorBufferSize = tensor.Create(TensorDataType::FLOAT32, static_cast<uint32_t>(size(tensorSizes)), tensorSizes);

		ElementWiseIdentity identityOperator = {};
		identityOperator.InputTensor = &tensor.GetTensorDesc();
		identityOperator.OutputTensor = &tensor.GetTensorDesc(); // Input and output tensors have same size/type.

		// Like Direct3D 12, these DESC structs don't need to be long-lived. This means, for example, that it's safe to place
		// the Operator (and all the subobjects it points to) on the stack, since they're no longer needed after
		// CreateOperator returns.

		// Compile the operator into an object that can be dispatched to the GPU. In this step, DirectML performs operator
		// fusion and just-in-time (JIT) compilation of shader bytecode, then compiles it into a Direct3D 12 pipeline state object (PSO).
		// The resulting compiled operator is a baked, optimized form of an operator suitable for execution on the GPU.
		N_RETURN(m_mlOperator.Create(m_mlDevice, OperatorDesc(OperatorType::ELEMENT_WISE_IDENTITY, &identityOperator)), false);
	}

	OperatorInitializer mlOperatorInitializer;
	mlOperatorInitializer.Create(m_mlDevice, &m_mlOperator, 1);

	// Query the operator for the required size (in descriptors) of its binding table.
	// You need to initialize an operator exactly once before it can be executed, and
	// the two stages require different numbers of descriptors for binding. For simplicity,
	// we create a single descriptor heap that's large enough to satisfy them both.
	const auto descriptorCount = mlOperatorInitializer.GetDescriptorCount();

	// Create descriptor heaps.
	N_RETURN(m_descriptorTableCache.AllocateDescriptorPool(CBV_SRV_UAV_POOL, descriptorCount), false);
	const auto descriptorHeap = m_descriptorTableCache.GetDescriptorPool(CBV_SRV_UAV_POOL);

	// Create a binding table over the descriptor heap we just created.
	Binding mlBinding;
	N_RETURN(mlBinding.Create(m_mlDevice, mlOperatorInitializer, descriptorHeap, descriptorCount), false);
	N_RETURN(m_mlBinding.Create(m_mlDevice, m_mlOperator, descriptorHeap, descriptorCount), false);

	// Create the temporary and persistent resources that are necessary for executing an operator.

	// The temporary resource is scratch memory (used internally by DirectML), whose contents you don't need to define.
	// The persistent resource is long-lived, and you need to initialize it using the IDMLOperatorInitializer.
	const auto temporaryResourceSize = mlOperatorInitializer.GetTemporaryResourceSize();
	const auto persistentResourceSize = m_mlOperator.GetPersistentResourceSize();

	// Bind and initialize the operator on the GPU.
	if (temporaryResourceSize != 0)
	{
		N_RETURN(m_temporaryBuffer.Create(m_device, temporaryResourceSize, ResourceFlag::NONE,
			MemoryType::DEFAULT, 0, nullptr, 0, nullptr, L"TemporaryBuffer"), false);
		mlBinding.BindTemporary(m_temporaryBuffer);
		m_mlBinding.BindTemporary(m_temporaryBuffer);
	}

	if (persistentResourceSize != 0)
	{
		N_RETURN(m_persistentBuffer.Create(m_device, persistentResourceSize, ResourceFlag::NONE,
			MemoryType::DEFAULT, 0, nullptr, 0, nullptr, L"PersistentBuffer"), false);
		mlBinding.AppendOutput(m_persistentBuffer);
		m_mlBinding.BindPersistent(m_persistentBuffer);
	}

	// Create tensor buffers for upload/input/output/readback of the tensor elements.
	{
		N_RETURN(m_inputBuffer.Create(m_device, m_tensorBufferSize, ResourceFlag::ALLOW_UNORDERED_ACCESS,
			MemoryType::DEFAULT, 0, nullptr, 0, nullptr, L"InputBuffer"), false);
		m_mlBinding.AppendInput(m_inputBuffer);
	}

	{
		N_RETURN(m_outputBuffer.Create(m_device, m_tensorBufferSize, ResourceFlag::ALLOW_UNORDERED_ACCESS,
			MemoryType::DEFAULT, 0, nullptr, 0, nullptr, L"OutputBuffer"), false);
		m_mlBinding.AppendOutput(m_outputBuffer);
	}

	// The command recorder is a stateless object that records Dispatches into an existing Direct3D 12 command list.
	ThrowIfFailed(m_mlDevice->CreateCommandRecorder(IID_PPV_ARGS(&m_mlCommandRecorder)));

	// Set the descriptor heap(s).
	const DescriptorPool descriptorPools[] = { descriptorHeap };
	m_commandList.SetDescriptorPools(static_cast<uint32_t>(size(descriptorPools)), descriptorPools);

	// Record execution of the operator initializer.
	m_mlCommandRecorder->RecordDispatch(m_commandList.GetCommandList().get(),
		mlOperatorInitializer.GetDispatchable().get(),
		mlBinding.GetDispatchableBindingTable().get());

	return true;
}

// Update frame-based values.
void DMLTest::OnUpdate()
{
	// Timer
	static auto time = 0.0, pauseTime = 0.0;

	m_timer.Tick();
	const auto totalTime = CalculateFrameStats();
	pauseTime = m_pausing ? totalTime - time : pauseTime;
	time = totalTime - pauseTime;
}

// Render the scene.
void DMLTest::OnRender()
{
	// Record all the commands we need to render the scene into the command list.
	PopulateCommandList();

	// Execute the command list.
	BaseCommandList* const ppCommandLists[] = { m_commandList.GetCommandList().get() };
	m_commandQueue->ExecuteCommandLists(static_cast<uint32_t>(size(ppCommandLists)), ppCommandLists);

	// Present the frame.
	ThrowIfFailed(m_swapChain->Present(0, 0));

	MoveToNextFrame();
}

void DMLTest::OnDestroy()
{
	// Ensure that the GPU is no longer referencing resources that are about to be
	// cleaned up by the destructor.
	WaitForGpu();

	CloseHandle(m_fenceEvent);
}

// User hot-key interactions.
void DMLTest::OnKeyUp(uint8_t key)
{
	switch (key)
	{
	case 0x20:	// case VK_SPACE:
		m_pausing = !m_pausing;
		break;
	case 0x70:	//case VK_F1:
		m_showFPS = !m_showFPS;
		break;
	}
}

void DMLTest::PopulateCommandList()
{
	// Command list allocators can only be reset when the associated 
	// command lists have finished execution on the GPU; apps should use 
	// fences to determine GPU execution progress.
	ThrowIfFailed(m_commandAllocators[m_frameIndex]->Reset());

	// However, when ExecuteCommandList() is called on a particular command 
	// list, that command list can then be reset at any time and must be before 
	// re-recording.
	ThrowIfFailed(m_commandList.Reset(m_commandAllocators[m_frameIndex], nullptr));

	// Indicate that the back buffer will be used as a render target.
	ResourceBarrier barriers[2];
	auto numBarriers = m_renderTargets[m_frameIndex].SetBarrier(barriers, ResourceState::RENDER_TARGET);
	numBarriers = m_inputBuffer.SetBarrier(barriers, ResourceState::UNORDERED_ACCESS, numBarriers);
	m_commandList.Barrier(numBarriers, barriers);

	// Record commands.
	//const float clearColor[] = { 0.2f, 0.2f, 0.4f, 0.0f };
	//m_commandList.ClearRenderTargetView(*m_rtvTables[m_frameIndex], clearColor);

	const auto bufferLength = static_cast<uint32_t>(m_tensorBufferSize / sizeof(float));
	m_commandList.SetComputePipelineLayout(m_pipelineLayouts[0]);
	m_commandList.SetComputeRootUnorderedAccessView(0, m_inputBuffer.GetResource());
	m_commandList.SetPipelineState(m_pipelines[0]);
	m_commandList.Dispatch(DIV_UP(bufferLength, 64), 1, 1);

	// 
	// Bind and execute the operator on the GPU.
	//

	const DescriptorPool descriptorPools[] = { m_descriptorTableCache.GetDescriptorPool(CBV_SRV_UAV_POOL) };
	m_commandList.SetDescriptorPools(static_cast<uint32_t>(size(descriptorPools)), descriptorPools);

	// Record execution of the compiled operator.
	numBarriers = m_outputBuffer.SetBarrier(barriers, ResourceState::UNORDERED_ACCESS);
	numBarriers = m_inputBuffer.SetBarrier(barriers, ResourceState::UNORDERED_ACCESS, numBarriers);
	m_commandList.Barrier(numBarriers, barriers);
	m_mlCommandRecorder->RecordDispatch(m_commandList.GetCommandList().get(),
		m_mlOperator.GetDispatchable().get(), m_mlBinding.GetDispatchableBindingTable().get());

	//
	// Read back
	//
	numBarriers = m_outputBuffer.SetBarrier(barriers, ResourceState::PIXEL_SHADER_RESOURCE);
	m_commandList.Barrier(numBarriers, barriers);

	// Set pipeline
	m_commandList.SetGraphicsPipelineLayout(m_pipelineLayouts[1]);
	m_commandList.SetGraphics32BitConstant(0, m_width, 0);
	m_commandList.SetGraphics32BitConstant(0, m_height, 1);
	m_commandList.SetGraphicsRootShaderResourceView(1, m_outputBuffer.GetResource());
	m_commandList.SetPipelineState(m_pipelines[1]);

	// Set viewport
	Viewport viewport(0.0f, 0.0f, static_cast<float>(m_width), static_cast<float>(m_height));
	RectRange scissorRect(0, 0, m_width, m_height);
	m_commandList.RSSetViewports(1, &viewport);
	m_commandList.RSSetScissorRects(1, &scissorRect);

	m_commandList.OMSetRenderTargets(1, &m_renderTargets[m_frameIndex].GetRTV());

	m_commandList.IASetPrimitiveTopology(PrimitiveTopology::TRIANGLELIST);
	m_commandList.Draw(3, 1, 0, 0);

	// Indicate that the back buffer will now be used to present.
	numBarriers = m_renderTargets[m_frameIndex].SetBarrier(barriers, ResourceState::PRESENT);
	m_commandList.Barrier(numBarriers, barriers);

	ThrowIfFailed(m_commandList.Close());
}

// Wait for pending GPU work to complete.
void DMLTest::WaitForGpu()
{
	// Schedule a Signal command in the queue.
	ThrowIfFailed(m_commandQueue->Signal(m_fence.get(), m_fenceValues[m_frameIndex]));

	// Wait until the fence has been processed.
	ThrowIfFailed(m_fence->SetEventOnCompletion(m_fenceValues[m_frameIndex], m_fenceEvent));
	WaitForSingleObjectEx(m_fenceEvent, INFINITE, FALSE);

	// Increment the fence value for the current frame.
	m_fenceValues[m_frameIndex]++;
}

// Prepare to render the next frame.
void DMLTest::MoveToNextFrame()
{
	// Schedule a Signal command in the queue.
	const auto currentFenceValue = m_fenceValues[m_frameIndex];
	ThrowIfFailed(m_commandQueue->Signal(m_fence.get(), currentFenceValue));

	// Update the frame index.
	m_frameIndex = m_swapChain->GetCurrentBackBufferIndex();

	// If the next frame is not ready to be rendered yet, wait until it is ready.
	if (m_fence->GetCompletedValue() < m_fenceValues[m_frameIndex])
	{
		ThrowIfFailed(m_fence->SetEventOnCompletion(m_fenceValues[m_frameIndex], m_fenceEvent));
		WaitForSingleObjectEx(m_fenceEvent, INFINITE, FALSE);
	}

	// Set the fence value for the next frame.
	m_fenceValues[m_frameIndex] = currentFenceValue + 1;
}

double DMLTest::CalculateFrameStats(float* pTimeStep)
{
	static int frameCnt = 0;
	static double elapsedTime = 0.0;
	static double previousTime = 0.0;
	const auto totalTime = m_timer.GetTotalSeconds();
	++frameCnt;

	const auto timeStep = static_cast<float>(totalTime - elapsedTime);

	// Compute averages over one second period.
	if ((totalTime - elapsedTime) >= 1.0f)
	{
		float fps = static_cast<float>(frameCnt) / timeStep;	// Normalize to an exact second.

		frameCnt = 0;
		elapsedTime = totalTime;

		wstringstream windowText;
		windowText << L"    fps: ";
		if (m_showFPS) windowText << setprecision(2) << fixed << fps;
		else windowText << L"[F1]";
		SetCustomWindowText(windowText.str().c_str());
	}

	if (pTimeStep)* pTimeStep = static_cast<float>(totalTime - previousTime);
	previousTime = totalTime;

	return totalTime;
}
