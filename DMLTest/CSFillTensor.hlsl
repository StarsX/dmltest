//--------------------------------------------------------------------------------------
// By XU, Tianchen
//--------------------------------------------------------------------------------------

RWStructuredBuffer<float> g_rwTensor;

[numthreads(64, 1, 1)]
void main(uint DTid : SV_DispatchThreadID)
{
	g_rwTensor[DTid] = DTid;
}
