//--------------------------------------------------------------------------------------
// By XU, Tianchen
//--------------------------------------------------------------------------------------

cbuffer cb
{
	uint2 g_viewport;
};

StructuredBuffer<float> g_roTensor;

float4 main(float4 pos : SV_POSITION) : SV_TARGET
{
	const uint2 loc = pos.xy;
	const uint idx = loc.y * g_viewport.x + loc.x;

	const float totalSize = g_viewport.x * g_viewport.y;
	const float val = g_roTensor[idx] / totalSize;

	return float4(val.xxx, 1.0);
}
